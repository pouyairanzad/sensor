package com.sematec.allowpopupinadriod;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView img;
    Context mContext = this;
    Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        img = findViewById(R.id.ImageCamera);
        findViewById(R.id.btnOpenCamera).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        checkCameraPermission();
    }

    void openCamera() {
        Intent bild = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(bild, 1500);
    }

    void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            openCamera();
        } else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.CAMERA},
                    1000);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1500) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Glide.with(mContext)
                        .load(photo)
                        .into(img);
            }
        }

    }
}
