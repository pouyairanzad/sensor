package com.sematec.allowpopupinadriod;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static com.sematec.allowpopupinadriod.R.id.ButtonGps;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText Name;
    EditText Family;
    EditText PhoneNumber;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        findViewById(R.id.ButtonInfo).setOnClickListener(this);
        findViewById(R.id.ButtonCamera).setOnClickListener(this);
        findViewById(ButtonGps).setOnClickListener(this);
        findViewById(R.id.ButtonSave).setOnClickListener(this);


    }

    void bind() {
        Name = findViewById(R.id.EditTextName);
        Family = findViewById(R.id.EditTextFamilyName);
        PhoneNumber = findViewById(R.id.EditTextPhone);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ButtonInfo) {


            Intent infoIntent = new Intent(mContext, InfoActivity.class);
            String name = Name.getText().toString();
            String family = Family.getText().toString();
            String phone = PhoneNumber.getText().toString();
            infoIntent.putExtra("name", name);
            infoIntent.putExtra("family", family);
            infoIntent.putExtra("phone", phone);
             startActivity(infoIntent);


        }

        if(view.getId()==R.id.ButtonCamera){
            Intent cameraIntent = new Intent(mContext,CameraActivity.class);
            startActivity(cameraIntent);
        }
     else if (view.getId()==R.id.ButtonGps){
         Intent SensorIntent = new Intent(mContext,Sensor.class);
         startActivity(SensorIntent);

        }



    }

}


